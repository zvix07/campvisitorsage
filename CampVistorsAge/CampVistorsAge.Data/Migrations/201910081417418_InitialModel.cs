﻿namespace CampVistorsAge.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CampGroups",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        YearOfVisit = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Visitors",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        BirthDate = c.DateTime(nullable: false),
                        CampGroup_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CampGroups", t => t.CampGroup_ID)
                .Index(t => t.CampGroup_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Visitors", "CampGroup_ID", "dbo.CampGroups");
            DropIndex("dbo.Visitors", new[] { "CampGroup_ID" });
            DropTable("dbo.Visitors");
            DropTable("dbo.CampGroups");
        }
    }
}
