﻿namespace CampVistorsAge.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMonthOfVisitColumsToCampGroupTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CampGroups", "MonthOfVisit", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CampGroups", "MonthOfVisit");
        }
    }
}
