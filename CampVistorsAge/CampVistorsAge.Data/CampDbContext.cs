﻿using System.Data.Entity;

namespace CampVistorsAge.Data
{
    public class CampDbContext : DbContext
    {
        public DbSet<Visitor> Visitors {get; set; }
        public  DbSet<CampGroup> CampGroups { get; set; }

        public CampDbContext():base("name=CampVisitorsConnection")
        {
                
        }
    }
}