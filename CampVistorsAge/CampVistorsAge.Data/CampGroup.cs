﻿using System.Collections;
using System.Collections.Generic;

namespace CampVistorsAge.Data
{
    public class CampGroup
    {
        public  int ID { get; set; }
        public string Name { get; set; }
        public  int YearOfVisit { get; set; }
        public int MonthOfVisit { get; set; }
        public IList<Visitor> VisitirCamp { get; set; }
    }
}