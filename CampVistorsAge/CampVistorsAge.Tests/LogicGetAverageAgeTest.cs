﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CampVistorsAge.Services;

namespace CampVistorsAge.Tests
{
    [TestClass]
    public class LogicGetAverageAgeTest
    {
        LogicGetAverageAge  logic = new LogicGetAverageAge();


        [TestMethod]
        public void CheckGetOldestVisitor()
        {
            string expectedName = "NameOldest";

            Visitor oldestVisitor = logic.GetOldestVisitor();

            Assert.AreEqual(expectedName, oldestVisitor.FirstName);
        }

        [TestMethod]
        public void CheckGetYoungestVisitor()
        {
            string expectedName = "NameYoungest";

            Visitor youngestVisitor = logic.GetYoungestVisitor();

            Assert.AreEqual(expectedName, youngestVisitor.FirstName);
        }

        [TestMethod]
        public void CheckAverageAgeOfVisitor()
        {
            int expected = 14; // порахував в екселі

            var averageAgeAll = logic.GetAverageAgeOfVisitors();

            Assert.AreEqual(expected, averageAgeAll);
        }

        [TestMethod]
        public void CheckAverageAgeGroupByYears()
        {
            bool correct = true;
            int expctedGroup1 = 13;
            int expectedGroup2 = 15;
            int keyGroup1 = 2015;
            int keyGroup2 = 2016;
            var result = logic.GetAverageAgeGroupByYears();

            if (result[keyGroup1] != expctedGroup1 || result[keyGroup2] != expectedGroup2)
                correct = false;

            Assert.IsTrue(correct);
        }

        [TestMethod]
        public  void CheckAverageAgeGroupByMonthOfCurrentYear()
        {
            bool correct = true;
            int expctedGroup1 = 15;
            int expectedGroup2 = 10;
            int keyGroup1 = 7;
            int keyGroup2 = 2;
            int year = 2015;

            var result = logic.GetAverageAgeGroupByMounthOfCurrentYear(year);

            if (result[keyGroup1] != expctedGroup1 || result[keyGroup2] != expectedGroup2)
                correct = false;

            Assert.IsTrue(correct);
        }
    }
}
