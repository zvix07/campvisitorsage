namespace CampVistorsAge.Services
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class CampModel : DbContext
    {
        public CampModel()
            : base("name=CampModelConnection")
        {
        }

        public virtual DbSet<CampGroup> CampGroups { get; set; }
        public virtual DbSet<Visitor> Visitors { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CampGroup>()
                .HasMany(e => e.Visitors)
                .WithOptional(e => e.CampGroup)
                .HasForeignKey(e => e.CampGroup_ID);
        }
    }
}
