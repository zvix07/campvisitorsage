﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampVistorsAge.Services
{
    public class LogicGetAverageAge
    {
       
        public int GetAverageAgeOfVisitors()
        {
            var context = new CampModel();

              var avg = context.Visitors.Average(c => (c.CampGroup.YearOfVisit - c.BirthDate.Year  ));
            return (int)avg;
        }

        public Dictionary<int,int> GetAverageAgeGroupByYears()
        {
            var context = new CampModel();

            var result = new Dictionary<int, int>();

            var groups = context.Visitors.GroupBy(c => c.CampGroup.YearOfVisit);

            foreach (var group in groups)
            {
                var averageAge = group.Average(c => (c.CampGroup.YearOfVisit - c.BirthDate.Year));
                result.Add(group.Key, (int) averageAge);
            }
            return result ;
        }

        public  Dictionary<int,int> GetAverageAgeGroupByMounthOfCurrentYear(int yearForGrouping)
        {
            var result = new Dictionary<int, int>();
            var context = new CampModel();
            var groups = context.Visitors.Where(c => c.CampGroup.YearOfVisit == yearForGrouping)
                                         .GroupBy(c => c.CampGroup.MonthOfVisit);
            foreach (var group in groups)
            {
                var averageAge = group.Average(c => (c.CampGroup.YearOfVisit - c.BirthDate.Year));
                result.Add(group.Key, (int)averageAge);
            }
            return result;
        }


        public Visitor GetOldestVisitor()  
        {
        
            var context = new CampModel();
           
            
           var   oldestVisitor  = context.Visitors.OrderByDescending(c => (c.CampGroup.YearOfVisit - c.BirthDate.Year ))
                                                .FirstOrDefault();
           return oldestVisitor;
        }

        public Visitor GetYoungestVisitor()
        {
            var context = new CampModel();


            var youngestVisitor = context.Visitors.OrderBy(c => (c.CampGroup.YearOfVisit - c.BirthDate.Year  ))
                                                .FirstOrDefault();
            return youngestVisitor;
        }
    }
}
