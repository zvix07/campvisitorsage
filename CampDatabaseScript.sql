USE [master]
GO
/****** Object:  Database [CampVisitorsDataBase]    Script Date: 10/9/2019 1:30:52 PM ******/
CREATE DATABASE [CampVisitorsDataBase]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CampVisitorsDataBase', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\CampVisitorsDataBase.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'CampVisitorsDataBase_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\CampVisitorsDataBase_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [CampVisitorsDataBase] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CampVisitorsDataBase].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CampVisitorsDataBase] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CampVisitorsDataBase] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CampVisitorsDataBase] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CampVisitorsDataBase] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CampVisitorsDataBase] SET ARITHABORT OFF 
GO
ALTER DATABASE [CampVisitorsDataBase] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CampVisitorsDataBase] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CampVisitorsDataBase] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CampVisitorsDataBase] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CampVisitorsDataBase] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CampVisitorsDataBase] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CampVisitorsDataBase] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CampVisitorsDataBase] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CampVisitorsDataBase] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CampVisitorsDataBase] SET  ENABLE_BROKER 
GO
ALTER DATABASE [CampVisitorsDataBase] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CampVisitorsDataBase] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CampVisitorsDataBase] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CampVisitorsDataBase] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CampVisitorsDataBase] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CampVisitorsDataBase] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [CampVisitorsDataBase] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CampVisitorsDataBase] SET RECOVERY FULL 
GO
ALTER DATABASE [CampVisitorsDataBase] SET  MULTI_USER 
GO
ALTER DATABASE [CampVisitorsDataBase] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CampVisitorsDataBase] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CampVisitorsDataBase] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CampVisitorsDataBase] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [CampVisitorsDataBase] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'CampVisitorsDataBase', N'ON'
GO
ALTER DATABASE [CampVisitorsDataBase] SET QUERY_STORE = OFF
GO
USE [CampVisitorsDataBase]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 10/9/2019 1:30:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CampGroups]    Script Date: 10/9/2019 1:30:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CampGroups](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[YearOfVisit] [int] NOT NULL,
	[MonthOfVisit] [int] NOT NULL,
 CONSTRAINT [PK_dbo.CampGroups] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Visitors]    Script Date: 10/9/2019 1:30:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Visitors](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[BirthDate] [datetime] NOT NULL,
	[CampGroup_ID] [int] NULL,
 CONSTRAINT [PK_dbo.Visitors] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Index [IX_CampGroup_ID]    Script Date: 10/9/2019 1:30:52 PM ******/
CREATE NONCLUSTERED INDEX [IX_CampGroup_ID] ON [dbo].[Visitors]
(
	[CampGroup_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CampGroups] ADD  DEFAULT ((0)) FOR [MonthOfVisit]
GO
ALTER TABLE [dbo].[Visitors]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Visitors_dbo.CampGroups_CampGroup_ID] FOREIGN KEY([CampGroup_ID])
REFERENCES [dbo].[CampGroups] ([ID])
GO
ALTER TABLE [dbo].[Visitors] CHECK CONSTRAINT [FK_dbo.Visitors_dbo.CampGroups_CampGroup_ID]
GO
USE [master]
GO
ALTER DATABASE [CampVisitorsDataBase] SET  READ_WRITE 
GO
